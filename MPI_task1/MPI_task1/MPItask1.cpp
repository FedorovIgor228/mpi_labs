﻿// MPItask1.cpp : Defines the entry point for the console application.
//
//>mpiicc - o test.exe C : \\"Program Files (x86)"\\IntelSWTools\\compilers_and_libraries_2018.0.124\\windows\\mpi\\test\\test.c
//for i = 0 i<EOS i++
//if char GLOBcount++ and flag = true
//	while(flag)
	//if ne char flag = false, i++
//else next


#include "stdafx.h"

#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <random>
#include <ctime>

using namespace std;

char* GetStr(int n)
{
	srand(time(0));
	char* str;
	str = new char[n];
	for (int i = 0; i < n; i++) {
		if (rand() % 2 == 0)
			str[i] = 'a';
		else
			str[i] = 'A';
		if (rand() % 5 == 0)
			str[i] = ' ';
	}
	return str;
}

int main(int argc, char* argv[])
{
	int procs, rankprocs, n = 50000000, N = 0; //num of elements in char*
	char* str = new char[n];
	int t1, t2;
	MPI_Status stat;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rankprocs);

	if (rankprocs == 0)
	{
		str = GetStr(n);
		cout << "Number proc : " << procs << endl;
		t1 = MPI_Wtime();
		cout << t1 << endl;
	}
	MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

	MPI_Bcast(str, n, MPI_CHAR, 0, MPI_COMM_WORLD);

	int size = n / procs;
	int a = size * rankprocs;
	int b = size * (rankprocs + 1);

	double sum = 0;
	cout << rankprocs << " proc start work" << endl;
	for (int i = a; i < b; i++)
	{
		if (str[i] == ' ')
			sum++;
		if (str[i] == ' ' && str[i + 1] == ' ' || str[i + 1] == '\0')
			sum--;
	}
	if (rankprocs == 0)
	{
		N = sum;
		
		if (str[0] != ' ')
			N++;
		
		for (int i = 1; i< procs; i++)
		{
			//MPI_redus;
			MPI_Recv(&sum, 1, MPI_DOUBLE, i, 1, MPI_COMM_WORLD, &stat);
			N += sum;
		}
		t2 = MPI_Wtime();
		cout << N <<"N" <<endl;
		cout << t2-t1 <<"   lol "<< endl;
	}
	else
		MPI_Send(&sum, 1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);

	MPI_Finalize();

	return 0;
} 

