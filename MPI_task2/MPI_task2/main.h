﻿#pragma once


	  typedef struct
	  {
		  unsigned int    bfType;
		  unsigned long   bfSize;
		  unsigned int    bfReserved1;
		  unsigned int    bfReserved2;
		  unsigned long   bfOffBits;
	  } BITMAPFILEHEADER;

	  typedef struct
	  {
		  unsigned int    biSize;
		  int             biWidth;
		  int             biHeight;
		  unsigned short  biPlanes;
		  unsigned short  biBitCount;
		  unsigned int    biCompression;
		  unsigned int    biSizeImage;
		  int             biXPelsPerMeter;
		  int             biYPelsPerMeter;
		  unsigned int    biClrUsed;
		  unsigned int    biClrImportant;
	  } BITMAPINFOHEADER;

	  typedef struct
	  {
		  int   rgbBlue;
		  int   rgbGreen;
		  int   rgbRed;
		  int   rgbReserved;
	  } RGBQUAD;

	  class ImageBMP {
	  public:
		  BITMAPFILEHEADER header;
		  BITMAPINFOHEADER bmiHeader;
		  RGBQUAD **rgb;

		  static unsigned short read_u16(FILE *fp);
		  static unsigned int read_u32(FILE *fp);
		  static int read_s32(FILE *fp);
		  static void write_u16(unsigned short input, FILE *fp);
		  static void write_u32(unsigned int input, FILE *fp);
		  static void write_s32(int input, FILE *fp);
	  };

	  using namespace std;
#define root_proc 0

#define WORD unsigned short 
#define DWORD unsigned long
#define LONG long
#define BYTE unsigned char
	  void ImageBMP::write_u16(unsigned short input, FILE *fp)
	  {
		  putc(input, fp);
		  putc(input >> 8, fp);
	  }

	  void ImageBMP::write_u32(unsigned int input, FILE *fp)
	  {
		  putc(input, fp);
		  putc(input >> 8, fp);
		  putc(input >> 16, fp);
		  putc(input >> 24, fp);
	  }

	  void ImageBMP::write_s32(int input, FILE *fp)
	  {
		  putc(input, fp);
		  putc(input >> 8, fp);
		  putc(input >> 16, fp);
		  putc(input >> 24, fp);
	  }

	  unsigned short ImageBMP::read_u16(FILE *fp)
	  {
		  unsigned char b0, b1;

		  b0 = getc(fp);
		  b1 = getc(fp);

		  return ((b1 << 8) | b0);
	  }

	  unsigned int ImageBMP::read_u32(FILE *fp)
	  {
		  unsigned char b0, b1, b2, b3;

		  b0 = getc(fp);
		  b1 = getc(fp);
		  b2 = getc(fp);
		  b3 = getc(fp);

		  return ((((((b3 << 8) | b2) << 8) | b1) << 8) | b0);
	  }

	  int ImageBMP::read_s32(FILE *fp)
	  {
		  unsigned char b0, b1, b2, b3;

		  b0 = getc(fp);
		  b1 = getc(fp);
		  b2 = getc(fp);
		  b3 = getc(fp);

		  return ((int)(((((b3 << 8) | b2) << 8) | b1) << 8) | b0);
	  }

	  ImageBMP* GetBMP(FILE* pFile) {
		  ImageBMP* img = new ImageBMP();
		  // считываем заголовок файла
		  //BITMAPFILEHEADER header;// attribute__((unused));
		  {
			  img->header.bfType = img->read_u16(pFile);
			  img->header.bfSize = img->read_u32(pFile);
			  img->header.bfReserved1 = img->read_u16(pFile);
			  img->header.bfReserved2 = img->read_u16(pFile);
			  img->header.bfOffBits = img->read_u32(pFile);
		  }
		  // считываем заголовок изображения
		  //BITMAPINFOHEADER bmiHeader;
		  {
			  img->bmiHeader.biSize = img->read_u32(pFile);
			  img->bmiHeader.biWidth = img->read_s32(pFile);
			  img->bmiHeader.biHeight = img->read_s32(pFile);
			  img->bmiHeader.biPlanes = img->read_u16(pFile);
			  img->bmiHeader.biBitCount = img->read_u16(pFile);
			  img->bmiHeader.biCompression = img->read_u32(pFile);
			  img->bmiHeader.biSizeImage = img->read_u32(pFile);
			  img->bmiHeader.biXPelsPerMeter = img->read_s32(pFile);
			  img->bmiHeader.biYPelsPerMeter = img->read_s32(pFile);
			  img->bmiHeader.biClrUsed = img->read_u32(pFile);
			  img->bmiHeader.biClrImportant = img->read_u32(pFile);
		  }

		  img->rgb = new RGBQUAD*[img->bmiHeader.biWidth];
		  for (int i = 0; i < img->bmiHeader.biWidth; i++) {
			  img->rgb[i] = new RGBQUAD[img->bmiHeader.biHeight];
		  }

		  for (int i = 0; i < img->bmiHeader.biWidth; i++) {
			  for (int j = 0; j < img->bmiHeader.biHeight; j++) {
				  img->rgb[i][j].rgbBlue = getc(pFile);
				  img->rgb[i][j].rgbGreen = getc(pFile);
				  img->rgb[i][j].rgbRed = getc(pFile);
			  }

			  // пропускаем последний байт в строке
			  getc(pFile);
		  }
		  return img;
	  }

	  void WriteBMP(ImageBMP* img) {
		  printf("Starting...\n");
		  FILE * pFile = fopen("C:\\Users\\figor\\Pictures\\file1.bmp", "wb");
		  int rem = (img->bmiHeader.biWidth % 4) == 0 ? 0 :
			  (4 - img->bmiHeader.biWidth % 4);
		  /*
		  // записываем заголовок файла
		  {
		  ImageBMP::write_u16(img->header.bfType, pFile);
		  img->header.bfSize = 54 + 1024 +
		  (img->bmiHeader.biWidth + rem)*
		  img->bmiHeader.biHeight;
		  ImageBMP::write_u32(img->header.bfSize, pFile);
		  ImageBMP::write_u16(img->header.bfReserved1, pFile);
		  ImageBMP::write_u16(img->header.bfReserved2, pFile);
		  img->header.bfOffBits = 54 + 1024;
		  ImageBMP::write_u32(img->header.bfOffBits, pFile);
		  }
		  // записываем заголовок изображения
		  {
		  ImageBMP::write_u32(img->bmiHeader.biSize, pFile);
		  ImageBMP::write_s32(img->bmiHeader.biWidth, pFile);
		  ImageBMP::write_s32(img->bmiHeader.biHeight, pFile);
		  ImageBMP::write_u16(img->bmiHeader.biPlanes, pFile);
		  ImageBMP::write_u16(img->bmiHeader.biBitCount, pFile);
		  ImageBMP::write_u32(img->bmiHeader.biCompression, pFile);
		  img->bmiHeader.biSizeImage = (
		  img->bmiHeader.biWidth + rem) *
		  img->bmiHeader.biHeight;
		  ImageBMP::write_u32(img->bmiHeader.biSizeImage, pFile);
		  ImageBMP::write_s32(img->bmiHeader.biXPelsPerMeter, pFile);
		  ImageBMP::write_s32(img->bmiHeader.biYPelsPerMeter, pFile);
		  ImageBMP::write_u32(img->bmiHeader.biClrUsed, pFile);
		  ImageBMP::write_u32(img->bmiHeader.biClrImportant, pFile);
		  }
		  // записываем байты цветов
		  /*
		  for (int i = 0; i < img->bmiHeader.biWidth; i++) {
		  for (int j = 0; j < img->bmiHeader.biHeight; j++) {
		  putc(~img->rgb[i][j].rgbBlue & 0xFF, pFile);
		  putc(~img->rgb[i][j].rgbGreen & 0xFF, pFile);
		  putc(~img->rgb[i][j].rgbRed & 0xFF, pFile);
		  }
		  }
		  */
		  BYTE *bitmapHeader = new BYTE[54];

		  // Create file
		  fopen_s(&pFile, "lol1", "wb");

		  // Row lenght must be a multiple of 4
		  rem = (img->bmiHeader.biWidth % 4) == 0 ? 0 :
			  (4 - img->bmiHeader.biWidth % 4);

		  // Set header info
		  *(WORD*)(bitmapHeader) = 0x4D42; // file type
		  *(DWORD*)(bitmapHeader + 2) = 54 + 1024 + (img->bmiHeader.biWidth + rem)
			  * img->bmiHeader.biHeight; //size in bytes of the bitmap file
		  *(WORD*)(bitmapHeader + 6) = 0; //reserved; must be 0
		  *(WORD*)(bitmapHeader + 8) = 0; //reserved; must be 0
		  *(DWORD*)(bitmapHeader + 10) = 54 + 1024; // offset in bytes to the bitmap bits
													// Set bitmap info
		  *(DWORD*)(bitmapHeader + 14) = 40; // number of bytes required by the struct
		  *(LONG*)(bitmapHeader + 18) = img->bmiHeader.biWidth; // width in pixels
		  *(LONG*)(bitmapHeader + 22) = img->bmiHeader.biHeight; // height in pixels
		  *(WORD*)(bitmapHeader + 26) = 1; // number of color planes, must be 1
		  *(WORD*)(bitmapHeader + 28) = 8; // number of bit per pixel
		  *(DWORD*)(bitmapHeader + 30) = 0; // type of compression
		  *(DWORD*)(bitmapHeader + 34) = (img->bmiHeader.biWidth + rem) *
			  img->bmiHeader.biHeight; // size of image in bytes
		  *(LONG*)(bitmapHeader + 38) = 0x0B88; // number of pixels per meter in x axis
		  *(LONG*)(bitmapHeader + 42) = 0x0B88; // number of pixels per meter in y axis
		  *(DWORD*)(bitmapHeader + 46) = 0; // number of colors used by the bitmap
		  *(DWORD*)(bitmapHeader + 50) = 0; // number of colors that are important

		  unsigned char *palette = new unsigned char[
			  img->bmiHeader.biWidth*img->bmiHeader.biHeight];
		  for (int i = 0; i < img->bmiHeader.biWidth*img->bmiHeader.biHeight; i += 4)
		  {
			  palette[i] = palette[i + 1] = palette[i + 2] = i / 4;
			  palette[i + 3] = 0;
		  }
		  fwrite(bitmapHeader, 1, 54, pFile);
		  fwrite(palette, sizeof(char), 1024, pFile);

		  fclose(pFile);
		  printf("Complete\n");
	  }
	  void SaveBitmap(ImageBMP* img)
	  {
		  // attempt to open the file specified
		  ofstream fout;

		  // attempt to open the file using binary access
		  fout.open("C:\\Users\\figor\\Pictures\\file1.bmp", ios::binary);

		  unsigned char red(0), green(0), blue(0);

		  if (fout.is_open())
		  {
			  // same as before, only outputting now
			  fout.write((char *)(&img->header), sizeof(BITMAPFILEHEADER));
			  fout.write((char *)(&img->bmiHeader), sizeof(BITMAPINFOHEADER));

			  // read off the color data in the bass ackwards MS way
			  for (unsigned int i = 0; i < img->bmiHeader.biWidth; i++) {
				  for (unsigned int j = 0; j < img->bmiHeader.biHeight; j++) {
					  red = (char)img->rgb[i][j].rgbRed;
					  green = (char)img->rgb[i][j].rgbGreen;
					  blue = (char)img->rgb[i][j].rgbBlue;
					  fout.write((const char *)(&blue), sizeof(blue));
					  fout.write((const char *)(&green), sizeof(green));
					  fout.write((const char *)(&red), sizeof(red));
				  }
			  }
			  // close the file
			  fout.close();
		  }
		  else
		  {
			  // post file not found message
			  cout << "What is this " << " you speak of?";
		  }
	  }
/*
	  int main()
	  {
		  FILE * pFile = fopen("C:\\Users\\figor\\Pictures\\file.bmp", "rb");
		  ImageBMP* image = GetBMP(pFile);
		  // выводим результат
		  //	for (int i = 0; i < image->bmiHeader.biWidth; i++) {
		  //	for (int j = 0; j < image->bmiHeader.biHeight; j++) {
		  //		printf("%d %d %d\n", image->rgb[i][j].rgbRed,
		  //		image->rgb[i][j].rgbGreen, image->rgb[i][j].rgbBlue);
		  //}
		  //	printf("\n");
		  //}
		  for (int i = 0; i < image->bmiHeader.biWidth; i++) {
			  for (int j = 0; j < image->bmiHeader.biHeight; j++) {
				  image->rgb[i][j].rgbRed = 0;
				  image->rgb[i][j].rgbGreen = 0;
				  image->rgb[i][j].rgbBlue = 0;
			  }
			  printf("\n");
		  }
		  WriteBMP(image);

		  //SaveBitmap(image);
		  //OutputGrayScaleImage("C:\\Users\\figor\\Pictures\\file1.bmp", 
		  //	((unsigned char*)image->rgb), image->bmiHeader.biWidth, image->bmiHeader.biHeight);
		  return 0;
	  }

	  */
