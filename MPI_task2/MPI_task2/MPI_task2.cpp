﻿// MPI_task2.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"

#include "mpi.h"
#include <stdio.h>
#include <iostream>
#include <random>
#include <ctime>
#include <stdlib.h>
#include <stdint.h>
#include<fstream>
#include <io.h>

#define root_proc 0

#define WORD unsigned short 
#define DWORD unsigned long
#define LONG long
#define BYTE unsigned char

void ReadBitmap24bpp(const char *filename, BYTE *parallelImage, BYTE *sequentialImage);
void OutputGrayScaleImage(const char *filename, BYTE *imageData, LONG width, LONG height);

int main(int argc, char* argv[])
{

	//W and H image;
	LONG width, height;
	//set contrast
	double contrastLevel = ::pow((100.0 + 200.0) / 100.0, 2);
	// Images
	BYTE *sequentialImage = 0, *parallelImage = 0;

	// MPI accessory variables
	int proc_num, proc_rank;
	int *send_counts, *displs;
	BYTE *recieve_buffer;
	int chunk_size;
	// Time variables
	double start_parallel, end_parallel;

	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD, &proc_num);
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);

	width = 1420;
	height = 1001;
	{
	// root_proc generates image
	if (proc_rank == root_proc)
	{
		sequentialImage = new BYTE[width * height];
		parallelImage = new BYTE[width * height];
		// Read image from file
		if (true)
		{
			std::cout << "Reading file\n";
			ReadBitmap24bpp("file.bmp", parallelImage, sequentialImage);
			std::cout << "Reading Finish\n";
		}
		// Generate random image
		if (false)
		{
			srand(MPI_Wtime() * 1000);
			for (long i = 0, k = 0; i < width * height; ++k, ++i)
			{
				sequentialImage[i] = rand() % (k % 200);
				parallelImage[i] = sequentialImage[i];
			}
		}

		// Output generated image
		OutputGrayScaleImage("__Original.bmp", sequentialImage, width, height);
			
		// Start parallel algorithm
		start_parallel = MPI_Wtime();

	}
	// Compute distributive parameters
	send_counts = new int[proc_num];
	displs = new int[proc_num];
	chunk_size = width * height / proc_num;
	for (int i = 0; i < proc_num; ++i)
	{
		send_counts[i] = chunk_size;
		displs[i] = i * chunk_size;
	}
	send_counts[proc_num - 1] += (width * height) % proc_num;

	// Distribute parts of the matrix to processes
	recieve_buffer = new BYTE[send_counts[proc_rank]];
	MPI_Scatterv(parallelImage, send_counts, displs,MPI_BYTE, 
		recieve_buffer, send_counts[proc_rank],	MPI_BYTE, root_proc, MPI_COMM_WORLD);
	double byte = 0;
	for (int k = 0; k < send_counts[proc_rank]; ++k) {
		byte = ((((recieve_buffer[k] / 255.0) - 0.5) *
			contrastLevel) + 0.5) * 255.0;
		if (byte > 255)
			byte = 255;
		if (byte < 0)
			byte = 0;
		recieve_buffer[k] = (BYTE)byte;
	}

	MPI_Gatherv(recieve_buffer, send_counts[proc_rank], MPI_BYTE,
		parallelImage, send_counts, displs, MPI_BYTE, root_proc, MPI_COMM_WORLD);

		//single ::
		if (proc_rank == root_proc)
		{
			// End of parallel algorithm
			end_parallel = MPI_Wtime();
			// Output new image
			std::cout << "Writing file\n";
			OutputGrayScaleImage("_New_p.bmp", parallelImage, width, height);
			// Execute sequential version of algorithm
			double start_sequential, end_sequential;
			start_sequential = MPI_Wtime();
			// Change contrast of each pixel
			double byte = 0;
			for (int k = 0; k < height*width; ++k) {
				byte = ((((sequentialImage[k] / 255.0) - 0.5) *
					contrastLevel) + 0.5) * 255.0;
				if (byte > 255)
					byte = 255;
				if (byte < 0)
					byte = 0;
				sequentialImage[k] = (BYTE)byte;
			}
			end_sequential = MPI_Wtime();
			OutputGrayScaleImage("_New_s.bmp", sequentialImage, width, height);
			std::cout << "Writing complete\n";
			
			std::cout << "Parallel version: "<< end_parallel - start_parallel<<" sec.\n" << std::endl;
			std::cout << "Sequential version: "<< end_sequential - start_sequential <<" sec.\n" << std::endl;
			std::cout << "Time gain: "<< end_sequential - start_sequential - end_parallel + start_parallel << " sec.\n" << std::endl;
			
			delete parallelImage;
			delete sequentialImage;
		}

		delete displs;
		delete send_counts;
		delete recieve_buffer;

		MPI_Finalize();
	}
	return 0;
}

void ReadBitmap24bpp(const char *filename, BYTE *parallelImage, BYTE *sequentialImage)
{
	// BMP file
	FILE *filePtr;
	// Open file
	fopen_s(&filePtr, filename, "rb");

	if (filePtr != NULL)
	{
		// BMP info
		BYTE *bitmapHeader = new BYTE[54];
		fread(bitmapHeader, 1, 54, filePtr);

		// Get dimensions of image
		int width = *(LONG*)(bitmapHeader + 18);
		int height = *(LONG*)(bitmapHeader + 22);

		for (int i = 0; i < width * height; ++i)
		{
			BYTE buff[3];
			fread(buff, 1, 3, filePtr);
			parallelImage[i] = (BYTE)(.2126 * buff[2] + .7152 * buff[1] + .0722 * buff[0]);
			sequentialImage[i] = parallelImage[i];
		}

		// Close file
		fclose(filePtr);
	}
}

void OutputGrayScaleImage(const char *filename, BYTE *imageData, LONG width, LONG height)
{
	// BMP file
	FILE * filePtr;
	// BMP info
	BYTE *bitmapHeader = new BYTE[54];
	// Create file
	fopen_s(&filePtr,filename, "wb");
	if (filePtr != NULL)
	{
		// Row lenght must be a multiple of 4
		int rem = (width % 4) == 0 ? 0 :
			(4 - width % 4);

		// Set header info
		*(WORD*)(bitmapHeader) = 0x4D42; // file type
		*(DWORD*)(bitmapHeader + 2) = 54 + 1024 + (width + rem)
			* height; //size in bytes of the bitmap file
		*(WORD*)(bitmapHeader + 6) = 0; //reserved; must be 0
		*(WORD*)(bitmapHeader + 8) = 0; //reserved; must be 0
		*(DWORD*)(bitmapHeader + 10) = 54 + 1024; // offset in bytes to the bitmap bits
												  // Set bitmap info
		*(DWORD*)(bitmapHeader + 14) = 40; // number of bytes required by the struct
		*(LONG*)(bitmapHeader + 18) = width; // width in pixels
		*(LONG*)(bitmapHeader + 22) = height; // height in pixels
		*(WORD*)(bitmapHeader + 26) = 1; // number of color planes, must be 1
		*(WORD*)(bitmapHeader + 28) = 8; // number of bit per pixel
		*(DWORD*)(bitmapHeader + 30) = 0; // type of compression
		*(DWORD*)(bitmapHeader + 34) = (width + rem) *
			height; // size of image in bytes
		*(LONG*)(bitmapHeader + 38) = 0x0B88; // number of pixels per meter in x axis
		*(LONG*)(bitmapHeader + 42) = 0x0B88; // number of pixels per meter in y axis
		*(DWORD*)(bitmapHeader + 46) = 0; // number of colors used by the bitmap
		*(DWORD*)(bitmapHeader + 50) = 0; // number of colors that are important
										  // Set palet
		BYTE palette[1024];
		for (int i = 0; i < 1024; i += 4)
		{
			palette[i] = palette[i + 1] = palette[i + 2] = i / 4;
			palette[i + 3] = 0;
		}
		// Write everything to file
		fwrite(bitmapHeader, 1, 54, filePtr);
		fwrite(palette, sizeof(BYTE), 1024, filePtr);

		if (rem == 0)
			fwrite(imageData, sizeof(BYTE), width * height, filePtr);

		// Close file
		fclose(filePtr);

	}

	delete bitmapHeader;
	std::cout << "LOL  " << filename << std::endl;
	return;
}




